import torch.nn as nn
import torch

class LSTM(nn.Module):
    def __init__(self, hidden_size, input_size, n_layers, batch_size, dropout):
        super(LSTM, self).__init__()

        # features in hidden state
        self.hidden_size = hidden_size

        # expected input size
        self.input_size = input_size

        self.dropout = dropout
        self.n_layers = n_layers
        self.batch_size = batch_size

        # initialize hidden state and cell state for first cell



        self.lstm = nn.LSTM(self.input_size, self.hidden_size, self.n_layers, dropout=self.dropout)
        self.softmax = nn.LogSoftmax(dim=1)

    def initialize_hidden_state(self):
        hidden_state = torch.randn(self.n_layers, self.batch_size, self.hidden_size)
        cell_state = torch.randn(self.n_layers, self.batch_size, self.hidden_size)

        self.hidden = (hidden_state, cell_state)

    def forward(self, sequence):
        # reshape input so that the LSTM accepts it
        sequence = sequence.view(self.batch_size, -1, len(sequence))

        output, self.hidden = self.lstm(sequence, self.hidden)
        return output.view(-1), self.hidden