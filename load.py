import numpy as np
import torch
from sklearn.preprocessing import LabelEncoder
from torch.utils.data import TensorDataset

def string_to_list(string):
    string = np.array(list(string))
    return string

## IF FLOAT IS NEEDED
def ordinal_encoder(array,label_encoder):
    label_encoder = LabelEncoder()
    label_encoder.fit(np.array(['A', 'C', 'G', 'T']))
    integer_encoded = label_encoder.transform(array)
    float_encoded = integer_encoded.astype(float)
    float_encoded[float_encoded == 0] = 0.25 # A
    float_encoded[float_encoded == 1] = 0.50 # C
    float_encoded[float_encoded == 2] = 0.75 # G
    float_encoded[float_encoded == 3] = 1.00 # T
    return float_encoded

## TEXT IN INT CONVERTER
def encode_text_int(array):
    label_encoder = LabelEncoder()
    label_encoder.fit(np.array(['A', 'C', 'G', 'T']))
    integer = label_encoder.transform(array)
    return integer

def convert(array):
    new_list = []
    for i in range(len(array)):
        new_list.append(encode_text_int(string_to_list(array[i])))
    
    return np.array(new_list)

def load():
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    with np.load('rnn-challenge-data.npz') as fh:
        train_x = fh['data_x']
        train_y = fh['data_y']
        val_x = fh['val_x']
        val_y = fh['val_y']
        test_x = fh['test_x']
    
    # train data
    train_x = convert(train_x)
    
    train_x = torch.Tensor(train_x).to(device)
    train_y = torch.Tensor(train_y).long().to(device)

    #validation data
    val_x = convert(val_x)

    val_x = torch.Tensor(val_x).to(device)
    val_y = torch.Tensor(val_y).to(device)
    
    # test data
    test_x = convert(test_x)
    #print(test)
    test_x = torch.Tensor(test_x).to(device)
    
    train_set = TensorDataset(train_x,train_y)
    val_set = TensorDataset(val_x, val_y)

    return train_set, val_set, test_x