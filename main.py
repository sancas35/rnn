import load
import lstm
import torch

def run_epoch():
    epoch_loss = 0
    for x, y in train_set:
        optimizer.zero_grad()

        model.initialize_hidden_state()

        out, hidden = model(x)

        out = out.view(-1, len(out))
        y = y.view(-1)

        loss =  loss_function(out, y)

        # keep track of total loss
        epoch_loss += loss.item()

        loss.backward()
        optimizer.step()
        optimizer.zero_grad()

    return epoch_loss

def train(EPOCHS):
    for i in range(EPOCHS):
        epoch_loss = run_epoch()
        print("total loss in epoch {}: {}".format(i+1, epoch_loss))


if __name__ == "__main__":
    train_set, val_set, test_x = load.load()
    print(train_set.tensors[0].shape)

    EPOCHS = 10

    model = lstm.LSTM(hidden_size=5, input_size=400, n_layers=1, dropout=0.1, batch_size=1)

    loss_function = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=0.005)

    train(EPOCHS)